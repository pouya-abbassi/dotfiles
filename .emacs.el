;;(setenv "http_proxy" "localhost:8123")
;;(setenv "https_proxy" "localhost:8123")

(setq start (float-time)) ;; To measure emacs startup time

(require 'package)
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("org" . "http://orgmode.org/elpa/")
                         ("melpa-stable" . "https://stable.melpa.org/packages/")))

(package-initialize)

;;(evil-mode)
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(setq inhibit-splash-screen +1)
(setq inhibit-startup-message +1)
(transient-mark-mode +1)
(load-theme 'badwolf +1)
(add-to-list 'default-frame-alist '(font . "Fira Code-12"))
(set-face-attribute 'default t :font "Fira Code-12")
;;(add-to-list 'default-frame-alist '(font . "Proggy Tiny TT-12"))
;;(set-face-attribute 'default t :font "Proggy Tiny TT-12")
;;(global-display-line-numbers-mode +1)
(setq visible-bell +1)
(setq show-paren-delay 0)
(show-paren-mode +1)
(icomplete-mode +1)
(ido-mode +1)

;;(define-key global-map "\C-h" 'paredit-open-curly)


;;(use-package evil
;;  :ensure t
;;  :config (evil-mode +1))

(use-package cider
  :ensure t)

(use-package projectile
  :ensure t
  :config
  (define-key projectile-mode-map (kbd "C-x p") 'projectile-command-map)
  (projectile-mode +1))

(use-package helm-projectile
  :ensure t
  :config (helm-projectile-on))

(use-package rainbow-delimiters
  :ensure t
  :config (rainbow-delimiters-mode +1))

(use-package paredit
  :ensure t
  :config (paredit-mode +1))

(use-package auto-complete
  :ensure t
  :config (auto-complete-mode +1))

(use-package nlinum
  :ensure t
  :config (global-nlinum-mode +1))

(use-package git-gutter-fringe
  :ensure t
  :config (global-git-gutter-mode +1))

(use-package  git-timemachine
  :ensure t)

(use-package nov
  :ensure t)

(use-package company
  :ensure t
  :config (company-mode +1))


(require 'rainbow-delimiters)
(require 'auto-complete)
(require 'git-gutter-fringe)


(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(git-gutter:added-sign "+")
 '(git-gutter:deleted-sign "-")
 '(git-gutter:lighter " GG")
 '(git-gutter:modified-sign " ")
 '(package-selected-packages
   (quote
    (cider cider-jack git-gutter-fringe use-package rainbow-delimiters paredit nov nlinum magit helm-projectile git-timemachine evil company clojure-mode badwolf-theme auto-complete))))


(add-hook 'clojure-mode-hook
	  'rainbow-delimiters-mode)

(add-hook 'clojure-mode-hook
	  'auto-complete-mode)

(add-hook 'clojure-mode-hook
	  'paredit-mode)

(add-hook 'clojure-mode-hook
	  'company-mode)

(add-hook 'clojure-mode-hook
	  'git-gutter-mode)

(add-hook 'clojure-mode-hook
	  'nlinum-mode)

(add-hook 'emacs-lisp-mode-hook
	  'rainbow-delimiters-mode)

(add-hook 'emacs-lisp-mode-hook
	  'auto-complete-mode)

(add-hook 'emacs-lisp-mode-hook
	  'paredit-mode)

(add-hook 'org-mode-hook
	  'flyspell-mode)

(add-hook 'markdown-mode-hook
	  'flyspell-mode)

(global-set-key (kbd "C-s") 'swiper)

(defun eval-this-sexp ()
  (interactive)
  (progn
    (paredit-forward-up)
    (call-interactively 'eval-last-sexp)
    (backward-char)))

(global-set-key (kbd "C-c p p") 'eval-this-sexp)

(message "Startup Time: %f" (- (float-time) start))

set nocompatible              " be iMproved, required
filetype off                  " required
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
Plugin 'bhurlow/vim-parinfer'
Plugin 'racer-rust/vim-racer'
call vundle#end()            " required
filetype plugin indent on    " required

set encoding=utf-8 " default encoding method
set number " show line number
set laststatus=2 " airline
let g:netrw_liststyle = 3 " airline
let g:netrw_banner = 0 " airline
syntax on " enable syntax highlight
set foldenable " enable folding
set foldmethod=indent " auto fold files based on indent
set foldlevel=99 " fold level
filetype indent on " load filetype-specific indent files
set background=dark " better contrast for dark background
set tabstop=2 " each <tab> will be 2 <space> width
set softtabstop=0 expandtab " use <space> instead of <tab>
set shiftwidth=2 " auto indent
set smarttab
%retab! " Retabulate the whole file
set showcmd " show command in bottom bar
set cursorline " highlight current line
hi CursorLine ctermbg=8 " set cursor line to gray
set wildmenu " visual autocomplete for command menu
set showmatch " highlight matching [{()}] 
set incsearch " search as characters are entered
set hlsearch " highlight matches
nnoremap <leader><space> :nohlsearch<CR> " use \<space> to clear search highlight
nnoremap gV `[v`] " visually select last inserted text
inoremap jk <esc> " map jk as <scape> " map jk as <scape>
nnoremap <leader>u :GundoToggle<cr> " map \u to toggle gundo
let mapleader="," " map , to \
:setlocal cm=blowfish2
:set colorcolumn=+1        " highlight column after 'textwidth'
:highlight ColorColumn ctermbg=lightgrey guibg=lightgrey
colorscheme badwolf " colorscheme
cnoremap w!! execute 'silent! write !sudo tee % >/dev/null' <bar> edit! " Type :w!! to save files that ned sudo access


let g:rbpt_colorpairs = [
    \ ['brown',       'RoyalBlue3'],
    \ ['Darkblue',    'SeaGreen3'],
    \ ['darkgray',    'DarkOrchid3'],
    \ ['darkgreen',   'firebrick3'],
    \ ['darkcyan',    'RoyalBlue3'],
    \ ['darkred',     'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['brown',       'firebrick3'],
    \ ['gray',        'RoyalBlue3'],
    \ ['black',       'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['Darkblue',    'firebrick3'],
    \ ['darkgreen',   'RoyalBlue3'],
    \ ['darkcyan',    'SeaGreen3'],
    \ ['darkred',     'DarkOrchid3'],
    \ ['red',         'firebrick3'],
    \ ]
let g:rbpt_max = 16
let g:rbpt_loadcmd_toggle = 0

au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces


"airline arrow characters
" air-line
let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'

" airline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''

let g:instant_markdown_autostart = 0
